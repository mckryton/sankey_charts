VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Steps_apply_colors"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public WithEvents ExecutionHooks As Senfgurke.TExecutionHooks
Attribute ExecutionHooks.VB_VarHelpID = -1

Public Sub Then_each_line_uses_a_different_color_than_th_933D5C6EA445(example_context As TContext)
    'And each line uses a different color than the line above
    Dim sankey_chart As ShapeRange
    Dim lines As ShapeRange
    Dim expected_line_count As Integer
    Dim line As Shape
    Dim above_color As Long
    Dim actual_color As Long
    
    Set sankey_chart = example_context.get_value("sankey_chart")
    Set lines = TSupport.getAllLinesfromChart(sankey_chart)
    above_color = -1
    For Each line In lines
        actual_color = line.line.ForeColor.RGB
        If above_color <> -1 Then
            tspec.expect(actual_color).not_to_be above_color
        End If
        above_color = actual_color
    Next
End Sub

Private Sub ExecutionHooks_AfterExampleHook(log_event As Senfgurke.TEvent)
    If log_event.SectionStatus = "OK" Then
        TSupport.close_unsaved_workbooks
    End If
End Sub
