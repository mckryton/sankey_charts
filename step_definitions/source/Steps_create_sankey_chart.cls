VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Steps_create_sankey_chart"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public WithEvents ExecutionHooks As Senfgurke.TExecutionHooks
Attribute ExecutionHooks.VB_VarHelpID = -1

Public Sub Given_a_sheet_with_a_table_showing_the_distri_E79A14C148C0(example_context As TContext, data_table As TDataTable)
    'Given a sheet with a table showing the distribution of city travelers
    Dim data_sheet As Worksheet
    
    Set data_sheet = TSupport.create_sheet_from_datatable(data_table)
    example_context.set_value data_sheet, "data_sheet"
    example_context.set_value data_table, "data_table"
End Sub

Public Sub When_a_sankey_chart_is_built_for_this_table_w_42AA862C7B5C(example_context As TContext, step_expressions As Collection)
    'When a Sankey chart is built for this table with a chart height of {integer} points
    Dim data_sheet As Worksheet
    Dim sankey_chart As ShapeRange
    
    Set data_sheet = example_context.get_value("data_sheet")
    Set sankey_chart = SankeyChart.Charts.Add(data_sheet, "A1", step_expressions(1))
    example_context.set_value sankey_chart, "sankey_chart"
End Sub

Public Sub When_a_sankey_chart_is_built_for_this_table_683A68E2F7C2(example_context As TContext)
    'When a Sankey chart is built for this table
    Dim data_sheet As Worksheet
    Dim sankey_chart As ShapeRange
    
    Set data_sheet = example_context.get_value("data_sheet")
    Set sankey_chart = SankeyChart.Charts.Add(data_sheet, "A1", 200)
    example_context.set_value sankey_chart, "sankey_chart"
End Sub

Public Sub Then_a_chart_will_be_added_to_the_sheet_7144E7E386D1(example_context As TContext)
    'Then a chart will be added to the sheet
    Dim sankey_chart As ShapeRange
    
    Set sankey_chart = example_context.get_value("sankey_chart")
    Senfgurke.tspec.expect(TypeName(sankey_chart)).to_be "ShapeRange"
End Sub

Public Sub Then_the_chart_shows_INT_lines_827D37A6CE02(example_context As TContext, step_expressions As Collection)
    'And the chart shows {integer} lines
    Dim sankey_chart As ShapeRange
    Dim lines As ShapeRange
    Dim expected_line_count As Integer
    
    Set sankey_chart = example_context.get_value("sankey_chart")
    Set lines = TSupport.getAllLinesfromChart(sankey_chart)
    expected_line_count = step_expressions(1)
    Senfgurke.tspec.expect(lines.Count).to_be expected_line_count
End Sub

Public Sub Then_the_height_of_each_path_line_with_square_047EF21E6B14(example_context As TContext, data_table As TDataTable)
    'And the height of each path (line with squares) corresponds to the share in the sum of all values
    Dim overall_sum As Long
    Dim squares As ShapeRange
    Dim lines As ShapeRange
    Dim sankey_chart As ShapeRange
    Dim start_square As Shape
    Dim end_square As Shape
    Dim line As Shape
    Dim expected_height As Long
    Dim line_index As Long
    
    Set sankey_chart = example_context.get_value("sankey_chart")
    Set lines = TSupport.getAllLinesfromChart(sankey_chart)
    line_index = 1
    For Each line In lines
        expected_height = data_table.table_rows(line_index)("path height")
        Senfgurke.tspec.expect(line.line.Weight).to_be expected_height
        Set start_square = line.ConnectorFormat.BeginConnectedShape
        tspec.expect(start_square.Height).to_be expected_height
        Set end_square = line.ConnectorFormat.EndConnectedShape
        tspec.expect(end_square.Height).to_be expected_height
        line_index = line_index + 1
    Next
End Sub

Public Sub Then_the_start_points_of_the_two_lines_are_se_0A47FD8D6ED1(example_context As TContext)
    'Then the start points of the two lines are separated by a gap
    Dim sankey_chart As ShapeRange
    Dim path1_start As Shape
    Dim path2_start As Shape
    
    Set sankey_chart = example_context.get_value("sankey_chart")
    Set path1_start = TSupport.getSquaresForLine(sankey_chart, "R2C1 start")(1)
    Set path2_start = TSupport.getSquaresForLine(sankey_chart, "R3C1 start")(1)
    Senfgurke.tspec.expect(path1_start.Top + path1_start.Height).not_to_be path2_start.Top
End Sub

Public Sub Then_the_start_point_of_the_two_lines_are_dra_2BCA12FB1A2C(example_context As TContext)
    'And the start point of the two lines are drawn next to each other
    Dim sankey_chart As ShapeRange
    Dim path1_start As Shape
    Dim path2_start As Shape
    
    Set sankey_chart = example_context.get_value("sankey_chart")
    Set path1_start = TSupport.getSquaresForLine(sankey_chart, "R2C1 start")(1)
    Set path2_start = TSupport.getSquaresForLine(sankey_chart, "R3C1 start")(1)
    Senfgurke.tspec.expect(path1_start.Top + path1_start.Height).to_be path2_start.Top
End Sub

Public Sub Then_the_end_point_of_the_two_lines_are_drawn_DCF4063B1A2C(example_context As TContext)
    'And the end point of the two lines are drawn next to each other
    Dim sankey_chart As ShapeRange
    Dim path1_end As Shape
    Dim path2_end As Shape
    
    Set sankey_chart = example_context.get_value("sankey_chart")
    Set path1_end = TSupport.getSquaresForLine(sankey_chart, "R2C1 end")(1)
    Set path2_end = TSupport.getSquaresForLine(sankey_chart, "R3C1 end")(1)
    Senfgurke.tspec.expect(path1_end.Top + path1_end.Height).to_be path2_end.Top
End Sub

Public Sub Then_the_end_points_of_the_two_lines_are_sepa_D5DDCA826ED1(example_context As TContext)
    'Then the end points of the two lines are separated by a gap
    Dim sankey_chart As ShapeRange
    Dim path1_end As Shape
    Dim path2_end As Shape
    
    Set sankey_chart = example_context.get_value("sankey_chart")
    Set path1_end = TSupport.getSquaresForLine(sankey_chart, "R2C1 end")(1)
    Set path2_end = TSupport.getSquaresForLine(sankey_chart, "R3C1 end")(1)
    Senfgurke.tspec.expect(path1_end.Top + path1_end.Height).not_to_be path2_end.Top
End Sub

Private Sub ExecutionHooks_AfterExampleHook(log_event As Senfgurke.TEvent)
    If log_event.SectionStatus = "OK" Then
        TSupport.close_unsaved_workbooks
    End If
End Sub
