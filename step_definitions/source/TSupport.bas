Attribute VB_Name = "TSupport"
'this module is for sharing helper functions between your step implementation classes

Option Explicit

Public Sub close_unsaved_workbooks()
    Dim test_book As Workbook

    For Each test_book In Application.Workbooks
        If Not test_book.Saved Then
            test_book.Close False
        End If
    Next
End Sub

Public Function create_sheet_from_datatable(data_table As TDataTable) As Worksheet
    Dim test_workbook As Workbook
    Dim data_sheet As Worksheet
    Dim col_name As Variant
    Dim data_row As Variant
    Dim rng_current As Range
    Dim col_index As Integer
    
    Set test_workbook = Workbooks.Add
    Set data_sheet = test_workbook.Sheets(1)
    Set rng_current = data_sheet.Range("A1")
    col_index = 0
    For Each col_name In data_table.column_names
        rng_current.Offset(, col_index).Value = col_name
        col_index = col_index + 1
    Next
    For Each data_row In data_table.table_rows
        Set rng_current = rng_current.Offset(1)
        col_index = 0
        For Each col_name In data_table.column_names
            rng_current.Offset(, col_index).Value = data_row(data_table.column_names(col_index + 1))
            col_index = col_index + 1
        Next
    Next
    Set create_sheet_from_datatable = data_sheet
End Function

Public Function getAllLinesfromChart(sankey_chart As ShapeRange) As ShapeRange
    Dim matching_names As String
    Dim member_shape As Shape 'Variant
    Dim filtered_shapes As ShapeRange
    
    matching_names = vbNullString
    For Each member_shape In sankey_chart
        If member_shape.Connector = True Then
            matching_names = matching_names & member_shape.Name & ","
        End If
    Next
    If matching_names <> vbNullString Then
        'remove the trailing comma
        matching_names = Left(matching_names, Len(matching_names) - 1)
        Set getAllLinesfromChart = sankey_chart.Parent.Shapes.Range(Split(matching_names, ","))
    Else
        Set getAllLinesfromChart = Nothing
    End If
End Function

Public Function getAllSquaresFromChart(sankey_chart As ShapeRange) As ShapeRange
    Set getAllSquaresFromChart = getSquares(sankey_chart)
End Function

Public Function getSquaresForLine(sankey_chart As ShapeRange, data_point_address As String) As ShapeRange
    Set getSquaresForLine = getSquares(sankey_chart, data_point_address)
End Function

Private Function getSquares(sankey_chart As ShapeRange, Optional shape_name_filter) As ShapeRange
    Dim matching_names As String
    Dim member_shape As Shape 'Variant
    Dim filtered_shapes As ShapeRange
    
    If IsMissing(shape_name_filter) Then shape_name_filter = vbNullString
    matching_names = vbNullString
    For Each member_shape In sankey_chart
        If member_shape.AutoShapeType = msoShapeRectangle And InStr(member_shape.Name, shape_name_filter) Then
            matching_names = matching_names & member_shape.Name & ","
        End If
    Next
    If matching_names <> vbNullString Then
        'remove the trailing comma
        matching_names = Left(matching_names, Len(matching_names) - 1)
        Set getSquares = sankey_chart.Parent.Shapes.Range(Split(matching_names, ","))
    Else
        Set getSquares = Nothing
    End If
End Function

