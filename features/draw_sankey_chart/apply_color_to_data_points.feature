Feature: apply color to data points
  If every line would use the same color it would be quite difficult to follow
  a specific data path. Therefore whenever a line represents another category
  the related should use another color than the line before.


  Rule: lines representing another category than the line above should use another color
    
    Scenario:
      Given a sheet with a table showing the distribution of city travelers
          | from      | share | to     |
          | Munich    |    45 | London |
          | Dresden   |     5 | London |
          | Lyon      |    15 | London |
       When a Sankey chart is built for this table
       Then the chart shows 3 lines
        And each line uses a different color than the line above
