Ability: draw data path lines
 A chart consists of lines connecting a square representing start and target
 with a width representing the fraction of all values.

  Rule: the height of a path is proportional to the share of its value to the sum of all values

    Scenario: one step table
      Given a sheet with a table showing the distribution of city travelers
          | from      | share | to     |
          | Munich    |    45 | London |
          | Dresden   |     5 | London |
          | Lyon      |    15 | London |
          | Barcelona |    35 | London |
       When a Sankey chart is built for this table with a chart height of 400 points
       Then a chart will be added to the sheet
        And the chart shows 4 lines
        And the height of each path (line with squares) corresponds to the share in the sum of all values
          | from      | share | to     | path height |
          | Munich    |    45 | London |         180 |
          | Dresden   |     5 | London |          20 |
          | Lyon      |    15 | London |          60 |
          | Barcelona |    35 | London |         140 |


  Rule: lines with different names are separated with gaps

    Scenario: two lines connect to the same target
      Given a sheet with a table showing the distribution of city travelers
          | from      | share | to     |
          | Munich    |    45 | London |
          | Dresden   |     5 | London |
       When a Sankey chart is built for this table
       Then the start points of the two lines are separated by a gap
        And the end point of the two lines are drawn next to each other

    Scenario: two lines connect to the same target
      Given a sheet with a table showing the distribution of city travelers
          | from      | share | to     |
          | Dresden   |    45 | Munich |
          | Dresden   |     5 | London |
       When a Sankey chart is built for this table
       Then the end points of the two lines are separated by a gap
        And the start point of the two lines are drawn next to each other
