Attribute VB_Name = "Charts"
Option Explicit

Public Function add(target_sheet As Worksheet, data_offset_address As String, chart_height As Long) As ShapeRange
    Dim chart_items As String
    Dim data_point_name As Range
    Dim last_data_point_name As Range
    Dim data_point_value As Range
    Dim data_value_sum As Long
    Dim path_y_start_offset As Long
    Dim path_y_end_offset As Long
    Dim path_height As Long
    Dim start_gap_height As Long
    Dim end_gap_height As Long
    Dim chart As ShapeRange
    
    'skip first row as data column header
    Set data_point_name = target_sheet.Range(data_offset_address).Offset(1)
    Set data_point_value = data_point_name.Offset(, 1)
    data_value_sum = WorksheetFunction.Sum(Range(data_point_value, data_point_value.Offset(data_point_value.CurrentRegion.Rows.Count - 1)))
    path_y_start_offset = 100
    path_y_end_offset = 100
    'loop through all rows with data
    Do While Len(data_point_name.Text) > 0
        path_height = (data_point_value / data_value_sum) * chart_height
        chart_items = chart_items & add_single_path(target_sheet, 282, path_y_start_offset, path_y_end_offset, _
            CHART_PATH_LENGTH, path_height, _
            "R" & data_point_name.Row & "C" & data_point_name.Column) & ","
        Set last_data_point_name = data_point_name
        Set data_point_name = data_point_name.Offset(1)
        Set data_point_value = data_point_name.Offset(, 1)
        'add gap only between start/end points with different names
        start_gap_height = 0
        If last_data_point_name.Text <> data_point_name.Text Then start_gap_height = CHART_GAP_HEIGHT
        end_gap_height = 0
        If last_data_point_name.Offset(, 2).Text <> data_point_name.Offset(, 2).Text Then end_gap_height = CHART_GAP_HEIGHT
        'set y position for startand end of the next data path
        path_y_start_offset = path_y_start_offset + path_height + start_gap_height
        path_y_end_offset = path_y_end_offset + path_height + end_gap_height
    Loop
    chart_items = Left(chart_items, Len(chart_items) - 1)
    Set chart = target_sheet.Shapes.Range(Split(chart_items, ","))
    Set add = chart
End Function

Private Function add_single_path(target_sheet As Worksheet, start_x As Long, start_y As Long, end_y As Long, _
         path_length As Long, path_height As Long, data_point_name_address As String) As String
    Dim chart_items As String
    Dim start_square As shape
    Dim end_square As shape
    Dim connector As shape
    
    chart_items = vbNullString
    Set start_square = target_sheet.Shapes.AddShape(msoShapeRectangle, start_x, start_y, CHART_SQUARE_WIDTH, path_height)
    start_square.Name = data_point_name_address & " start"
    chart_items = chart_items & start_square.Name & ","
    Set end_square = target_sheet.Shapes.AddShape(msoShapeRectangle, start_x + path_length, end_y, CHART_SQUARE_WIDTH, path_height)
    end_square.Name = data_point_name_address & " end"
    chart_items = chart_items & end_square.Name & ","
    Set connector = target_sheet.Shapes.AddConnector(msoConnectorCurve, 362, 116, 434, 188)
    With connector
        .ConnectorFormat.BeginConnect start_square, 4
        .ConnectorFormat.EndConnect end_square, 2
        .Line.Weight = path_height
    End With
    chart_items = chart_items & connector.Name & ","
    add_single_path = Left(chart_items, Len(chart_items) - 1)
End Function
