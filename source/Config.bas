Attribute VB_Name = "Config"
Option Explicit

' see ErrorDef module for error related constants

' chart height does not include gaps between data paths
Public Const CHART_GAP_HEIGHT = 20
Public Const CHART_PATH_LENGTH = 400
Public Const CHART_SQUARE_WIDTH = 50
